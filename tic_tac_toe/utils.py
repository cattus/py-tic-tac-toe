
import logging


__all__ = [
    'LoggerMixin',
    'intersperse',
]


class LoggerMixin:
    def __init__(self, *args, **kwargs):
        self._logger = logging.getLogger(f'{self.__class__.__module__}.{self.__class__.__name__}')
        self._logger.debug('LoggerMixin init')


def intersperse(iterable, item=None, item_factory=None):
    def factory(x):
        if item:
            return item
        elif item_factory:
            return item_factory(x)

    is_first = True
    for x in iterable:
        if not is_first:
            i = factory(x)
            if i:
                yield i
        is_first = False
        yield x
