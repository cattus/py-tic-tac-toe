import re
from typing import List, Optional, Type, Callable, Any

import sys

import logging
import asyncio
import urwid

from urwid import \
    LineBox, BigText, Font, Padding, Columns, \
    Pile, WidgetWrap, AttrMap, Filler, Button, \
    Widget, Text, SolidFill, \
    BoxAdapter, WidgetPlaceholder, HalfBlock7x7Font, HalfBlock5x4Font, Overlay, Edit, Divider


from tic_tac_toe import Chip, UserInfo, UserStats, GameResult
from tic_tac_toe.board import Board
from tic_tac_toe.utils import LoggerMixin
from tic_tac_toe.net_commons import ConnectionHandler, ConnectionListener

from tic_tac_toe.protocol import \
    Message, HelloMessage, LoginMessage, UserInfoMessage, \
    StartGameMessage, GameStartedMessage, \
    TurnMessage, GameOverMessage, ErrorMessage, \
    DecodingError, TextProtocol


class TTOEFont(Font):
    height = 7
    width = 13
    data = [u"""
XXXXXXXXXXXXX             OOOOOOOOOOOOOxxxxxxxxxxxxxooooooooooooo
▐█         █▌               ▄███████▄                            
  █▌     ▐█                █         █                           
   ▐█   █▌                ▐█   █     █▌    ▀▄ ▄▀        ▄▀▀▀▄    
     ▐█▌                  ▐█    █    █▌      █         ▐█   █▌   
   ▐█   █▌                ▐█     █   █▌    ▄▀ ▀▄        ▀▄▄▄▀    
  █▌     ▐█                █         █                           
▐█         █▌               ▀███████▀                            
"""]


urwid.font.add_font("TTOEFont", TTOEFont)


class ColorName:
    chip = 'chip'
    chip_focused = 'chip-selected'
    my_chip = 'my-chip'
    my_chip_focused = 'my-chip-selected'
    my_turn_focused = 'my-turn-focused'
    their_chip = 'their-chip'
    their_chip_focused = 'their-chip-focused'
    empty_cell = 'empty-cell'
    empty_cell_focused = 'empty-cell-focused'
    turn_o = 'turn-o'
    turn_x = 'turn-x'
    dialog = 'dialog'
    bg = 'bg'



palette = [
    (ColorName.chip,               'white',      'default'),
    (ColorName.chip_focused,       'white',      'dark red'),
    (ColorName.my_chip,            'dark green', 'default'),
    (ColorName.my_chip_focused,    'dark green', 'dark red'),
    (ColorName.my_turn_focused,    'dark green', 'default'),
    (ColorName.their_chip,         'light gray', 'default'),
    (ColorName.their_chip_focused, 'light gray', 'dark red'),
    (ColorName.dialog,             'white',      'dark blue'),
    (ColorName.bg,             'default', 'default')
]

_font_7x7 = HalfBlock7x7Font()
_font_5x4 = HalfBlock5x4Font()
_font_ttoe = TTOEFont()


class BoardCell(Widget, LoggerMixin):
    _selectable = True
    _sizing = frozenset([urwid.FIXED])
    _font = _font_ttoe

    signal_activate = 'activate'
    signals = [signal_activate]

    def __init__(self, player_chip: Chip = Chip.X, on_activate=None):
        super().__init__()
        self._on_activate = on_activate
        self._disabled = True
        self._chip: Optional[Chip] = None
        self._player_chip = player_chip
        self._text = self._mk_text()
        self._text_focused = self._mk_text()
        self._update_text()

    def _mk_text(self):
        return Padding(BigText((ColorName.empty_cell, ' '), self._font), width=urwid.CLIP)

    def _set_text(self, markup):
        self._text.base_widget.set_text(markup)

    def _set_text_focused(self, markup):
        self._text_focused.base_widget.set_text(markup)

    @property
    def disabled(self) -> bool:
        return self._disabled

    @disabled.setter
    def disabled(self, disabled: bool):
        self._logger.debug(f'set_disabled: {disabled}')
        self._disabled = disabled
        self._update_text()

    @property
    def chip(self) -> Optional[Chip]:
        return self._chip

    @chip.setter
    def chip(self, chip: Chip):
        assert isinstance(chip, Chip)
        self._chip = chip
        self._update_text()

    def _update_text(self):
        if self._chip == Chip.X:
            text = 'X'
            text_focused = 'X'
        elif self._chip == Chip.O:
            text = 'O'
            text_focused = 'O'
        else:
            text = ' '
            if self._disabled:
                text_focused = ' '
            else:
                text_focused = 'x' if self._player_chip == Chip.X else 'o'

        if self._chip is None:
            color = ColorName.empty_cell
            if self._disabled:
                color_focused = ColorName.empty_cell_focused
            else:
                color_focused = ColorName.my_turn_focused
        elif self._chip == self._player_chip:
            color = ColorName.my_chip
            color_focused = ColorName.my_chip_focused
        else:
            color = ColorName.their_chip
            color_focused = ColorName.their_chip_focused

        self._set_text((color, text))
        self._set_text_focused((color_focused, text_focused))

    def render(self, size, focus=False):
        size = (self._font.width, self._font.height)
        if focus:
            return self._text_focused.render(size, focus)
        return self._text.render(size, focus)

    def keypress(self, size, key):
        self._logger.debug(f'BoardCell key: {key}')
        if key == 'enter' and not self.disabled:
            if self._chip is None:
                self.chip = self._player_chip
                self._emit(self.signal_activate)
                if self._on_activate:
                    self._on_activate()
            else:
                sys.stdout.write('\a')
        else:
            return key

    def mouse_event(self, size, event, button, x, y, focus):
        if button == 1 and urwid.util.is_mouse_press(event):
            if self._chip is None:
                self.chip = self._player_chip
            else:
                sys.stdout.write('\a')

    def pack(self, size, focus=False):
        return self._font.width, self._font.height

    def rows(self, size, focus=False):
        return self._font.height

    def reset(self, player_chip):
        self._player_chip = player_chip
        self._disabled = True
        self._chip = None
        self._update_text()


class GameBoardWidget(WidgetWrap, LoggerMixin):
    _sizing = frozenset([urwid.FIXED])

    signal_activate = 'activate'
    signals = [signal_activate]

    def __init__(self):
        LoggerMixin.__init__(self)
        self._disabled = True
        self._cells: List[BoardCell] = []
        columns = []

        for row in range(Board.edge_size):
            cells_row = [self._create_cell(row, col) for col in range(Board.edge_size)]
            self._cells += (x.base_widget for x in cells_row)
            cell_width = cells_row[0].base_widget.pack(())[0]
            cells_with_width = [
                (cell_width + 2 if i == Board.edge_size - 1 else cell_width + 1, cells_row[i])
                for i in range(Board.edge_size)
            ]
            columns.append(Columns(cells_with_width))

        grid = Pile(columns)
        WidgetWrap.__init__(self, grid)

    def set_cell(self, cell: int, chip: Chip):
        self._cells[cell - 1].chip = chip

    def pack(self, size, focus=False):
        pack = super().pack(size, focus)
        self._logger.debug(f'pack: {pack}')
        return super().pack(size, focus)

    def reset(self, player_chip: Chip):
        for c in self._cells:
            c.reset(player_chip)

    @property
    def disabled(self) -> bool:
        return self._disabled

    @disabled.setter
    def disabled(self, disabled: bool):
        self._logger.debug(f'set_disabled: {disabled}')
        self._disabled = disabled
        for c in self._cells:
            c.disabled = disabled

    def connect_signal_activate(self, callback):
        urwid.connect_signal(self, self.signal_activate, callback)

    def _activate(self, cell: int):
        self._emit(self.signal_activate, cell)

    def _create_cell(self, row, col):
        is_top = row == 0
        is_bottom = row == Board.edge_size - 1
        is_left = col == 0
        is_right = col == Board.edge_size - 1

        tline = '─'
        lline = '│'
        tlcorner = '┼'
        trcorner= '─'
        blcorner= '─'
        rline = None
        bline = None
        brcorner = '─'

        if is_left and is_top:
            tlcorner = '┌'
        elif is_left:
            tlcorner = '├'
        elif is_top:
            tlcorner = '┬'

        if is_right:
            trcorner = '┐' if is_top else '┤'
            rline = '│'

        if is_bottom:
            blcorner = '└' if is_left else '┴'
            brcorner = '┘' if is_right else '─'
            bline = '─'

        cell = BoardCell(
            Chip.X,
            on_activate=lambda *args: self._activate(row * Board.edge_size + col + 1))

        box = LineBox(
            Padding(cell, width=urwid.PACK),
            tlcorner=tlcorner,
            tline=tline,
            lline=lline,
            trcorner=trcorner,
            blcorner=blcorner,
            rline=rline,
            bline=bline,
            brcorner=brcorner)

        return AttrMap(box, ColorName.chip)


class UserStatsWidget(WidgetWrap):
    def __init__(self):
        self._wins_text = BigText('', _font_5x4)
        self._draws_text = BigText('', _font_5x4)
        self._loses_text = BigText('', _font_5x4)

        view = Columns([
            (5 * 6,
             Pile([
                Padding(BigText('Wins:', _font_5x4), width=urwid.CLIP, align=urwid.LEFT),
                Padding(BigText('Draws:', _font_5x4), width=urwid.CLIP, align=urwid.LEFT),
                Padding(BigText('Loses:', _font_5x4), width=urwid.CLIP, align=urwid.LEFT)
             ])),
            (5 * 6,
             Pile([
                Padding(self._wins_text, width=urwid.CLIP, align=urwid.LEFT),
                Padding(self._draws_text, width=urwid.CLIP, align=urwid.LEFT),
                Padding(self._loses_text, width=urwid.CLIP, align=urwid.LEFT)
             ]))
        ])

        super().__init__(view)

    def set(self, stat: UserStats):
        self._wins_text.set_text(str(stat.wins))
        self._draws_text.set_text(str(stat.draws))
        self._loses_text.set_text(str(stat.loses))


class OverlayDialog(WidgetWrap):
    def __init__(self, body: Widget, under: Widget, title: str = ''):
        dialog = AttrMap(LineBox(body, title=title, title_align=urwid.LEFT), ColorName.dialog)
        overlay = Overlay(dialog, under,
                          align=urwid.CENTER, width=(urwid.RELATIVE, 60),
                          valign=urwid.MIDDLE, height=urwid.PACK)

        super().__init__(overlay)


class TextDialog(OverlayDialog):
    def __init__(self, under: Widget, text: str, title: str = '', buttons_line=None):
        view = [
            Divider(),
            Padding(Text(text), align=urwid.CENTER),
            Divider()
        ]
        if buttons_line:
            view.append(buttons_line)

        body = Pile(view)

        super().__init__(body, under, title=title)


class GameWidget(WidgetWrap, LoggerMixin):
    _banner_title = 'Tic Tac Toe'

    def __init__(self):
        LoggerMixin.__init__(self)

        self._player_info: Optional[UserInfo] = None
        self._opponent_info: Optional[UserInfo] = None
        self._player_chip: Optional[Chip] = None

        self._player_text = BigText('', _font_5x4)
        self._opponent_text = BigText('', _font_5x4)
        self._player_chip_text = BigText(' ', _font_5x4)
        self._opponent_chip_text = BigText(' ', _font_5x4)

        self._game_board = GameBoardWidget()
        self._stats_widget = UserStatsWidget()

        main_view = Pile([
            BoxAdapter(SolidFill(), 1),
            Padding(BigText(self._banner_title, _font_7x7), width=urwid.CLIP, align=urwid.CENTER),
            BoxAdapter(SolidFill(), 1),
            BoxAdapter(SolidFill('▚'), 1),
            BoxAdapter(SolidFill(), 1),
            Columns([
                Padding(self._player_text, width=urwid.CLIP, align=urwid.LEFT),
                # (2, BoxAdapter(SolidFill(), 1)),
                Columns([
                    Padding(self._player_chip_text, width=urwid.CLIP, align=urwid.RIGHT),
                    Padding(BigText('VS', _font_5x4), width=urwid.CLIP, align=urwid.CENTER),
                    Padding(self._opponent_chip_text, width=urwid.CLIP, align=urwid.LEFT)
                ]),
                Padding(self._opponent_text, width=urwid.CLIP, align=urwid.RIGHT),
            ]),
            BoxAdapter(SolidFill(), 2),
            Columns([self._stats_widget, self._game_board, UserStatsWidget()])
        ])

        WidgetWrap.__init__(self, Filler(main_view))

    def opponent_turn(self, cell):
        self._game_board.set_cell(cell, Chip.O if self._player_chip == Chip.X else Chip.X)

    @property
    def opponent(self) -> Optional[UserInfo]:
        return self._opponent_info

    @opponent.setter
    def opponent(self, value: UserInfo):
        self._opponent_info = value
        self._opponent_text.set_text(value.name)

    @property
    def player(self) -> Optional[UserInfo]:
        return self._player_info

    @player.setter
    def player(self, value: UserInfo):
        self._player_info = value
        self._player_text.set_text(value.name)
        self._stats_widget.set(value.stats)

    @property
    def player_chip(self) -> Optional[Chip]:
        return self._player_chip

    def enable(self):
        self._game_board.disabled = False

    def disable(self):
        self._game_board.disabled = True

    def game_over(self, result: GameResult):
        if result == GameResult.XO:
            self._player_info.stats.draws += 1
        elif result == GameResult.X and self._player_chip == Chip.X:
            self._player_info.stats.wins += 1
        else:
            self._player_info.stats.loses += 1

        self._stats_widget.set(self._player_info.stats)

    def set_game(self, chip: Chip, opponent: UserInfo):
        self.opponent = opponent
        self._player_chip = chip
        self._player_chip_text.set_text('X' if chip == Chip.X else 'O')
        self._opponent_chip_text.set_text('O' if chip == Chip.X else 'X')
        self._game_board.reset(chip)
        self._game_board.disabled = True

    def connect_signal_activate(self, callback):
        self._game_board.connect_signal_activate(callback)


logger = logging.getLogger(f'{__name__}')


class ClientGameController(LoggerMixin):
    def __init__(self, main_widget: WidgetPlaceholder, loop: urwid.MainLoop) -> None:
        super().__init__(TextProtocol())
        self._connection: Optional[ConnectionHandler] = None
        self._game_widget = GameWidget()
        self._main_widget = main_widget
        self._receive_loop_task: Optional[asyncio.Task] = None
        self._is_logged_in = False
        self._urwid_main_loop = loop

    @property
    def player(self) -> UserInfo:
        return self._game_widget.player

    @player.setter
    def player(self, player: UserInfo):
        self._game_widget.player = player

    @property
    def connection(self) -> ConnectionHandler:
        return self._connection

    @connection.setter
    def connection(self, connection: ConnectionHandler):
        self._connection = connection

    @property
    def game_widget(self) -> GameWidget:
        return self._game_widget

    @property
    def widget(self) -> Widget:
        return self._main_widget.original_widget

    @widget.setter
    def widget(self, widget: Widget):
        self._main_widget.original_widget = widget

    def send(self, message: Message):
        self._connection.send(message)

    def show(self, widget: Widget):
        self._main_widget.original_widget = widget

    def set_game(self, x_player_name: str, o_player_name: str):
        if x_player_name == self.game_widget.player.name:
            self.game_widget.set_game(Chip.X, UserInfo(o_player_name))
            self.game_widget.enable()
        else:
            self.game_widget.set_game(Chip.O, UserInfo(x_player_name))

    def exit(self):
        self._logger.info('Exit called')
        if self._connection:
            self._logger.info('Closing connection')
            self._connection.close()
        self._logger.info('Stopping main loop')
        raise urwid.ExitMainLoop()


class Behavior(ConnectionListener, LoggerMixin):
    def __init__(self, controller: ClientGameController):
        super().__init__()
        self._controller = controller
        if self._controller.connection:
            self._controller.connection.listener = self
        self._logger.debug(f'Starting behavior {type(self).__name__}')

    def on_unexpected_error(self, exception: Exception):
        self._logger.error('Unexpected error', exc_info=exception)
        self._controller.exit()

    def on_disconnected(self):
        self._logger.error('Disconnected')
        self._controller.exit()

    def on_decoding_error(self, exception: DecodingError):
        self._logger.error('Decoding error', exc_info=exception)
        self._controller.exit()

    def on_unexpected_message(self, message: Message):
        self._logger.error(f'Unexpected message: {message}')
        self._controller.exit()

    def on_message_error(self, message: ErrorMessage):
        close_button = Button('Close', lambda *args: self._controller.exit())

        buttons = Columns([(10, close_button)])

        dialog = TextDialog(self._controller.widget, message.message, title='Error', buttons_line=buttons)

        self._controller.show(dialog)


class CommonStartGameBehavior(Behavior):
    def __init__(self, controller: ClientGameController):
        super().__init__(controller)

    def _show_waiting_game_dialog(self):
        dialog = TextDialog(self._controller.game_widget, 'Waiting for opponent player...')
        self._controller.show(dialog)

    def _start_game(self):
        self._show_waiting_game_dialog()
        self._controller.send(StartGameMessage())


class GameBehavior(CommonStartGameBehavior):
    def __init__(self, controller: ClientGameController):
        super().__init__(controller)
        self._controller.game_widget.connect_signal_activate(self.on_cell_activated)
        self._controller.show(self._controller.game_widget)

    def on_cell_activated(self, widget, cell):
        self._controller.game_widget.disable()
        self._controller.send(TurnMessage(cell))

    def on_message_turn(self, message: TurnMessage):
        self._controller.game_widget.opponent_turn(message.cell)
        self._controller.game_widget.enable()

    def on_message_game_over(self, message: GameOverMessage):
        self._controller.game_widget.game_over(message.result)
        exit_button = Button('Exit', lambda *args: self._controller.exit())
        start_button = Button('Play again', lambda *args: self._start_game())

        if message.result == GameResult.XO:
            text = 'Draw!'
        elif message.result == GameResult.X and self._controller.game_widget.player_chip == Chip.X \
                or message.result == GameResult.O and self._controller.game_widget.player_chip == Chip.O:
            text = 'You Win!'
        else:
            text = 'You Loose!'

        body = Pile([
            Divider(),
            Padding(BigText(text, _font_7x7), width=urwid.CLIP, align=urwid.CENTER),
            Divider(),
            Columns([(15, start_button), (10, exit_button)])
        ])

        dialog = OverlayDialog(body, self._controller.game_widget)

        self._controller.show(dialog)

    def on_message_user_info(self, message: UserInfoMessage):
        self._controller.player = UserInfo(
            message.name,
            UserStats(message.wins, message.draws, message.loses))

    def on_message_game_started(self, message: GameStartedMessage):
        self._controller.set_game(message.x_player_name, message.o_player_name)
        self._controller.show(self._controller.game_widget)


class StartGameBehavior(CommonStartGameBehavior):
    def __init__(self, controller: ClientGameController):
        super().__init__(controller)
        self.start_game_dialog()

    def start_game_dialog(self):
        exit_button = Button('Exit', lambda *args: self._controller.exit())
        start_button = Button('Start', lambda *args: self._start_game())

        buttons = Columns([(10, start_button), (10, exit_button)])
        dialog = TextDialog(self._controller.game_widget,
                            text='Start new game?',
                            buttons_line=buttons)

        self._controller.show(dialog)

    def on_message_game_started(self, message: GameStartedMessage):
        self._controller.set_game(message.x_player_name, message.o_player_name)
        GameBehavior(self._controller)


class LoginBehavior(Behavior):

    _username_regex = re.compile(r'^\w{1,10}$')

    def __init__(self, controller: ClientGameController):
        super().__init__(controller)
        self._show_login_dialog()

    def _show_login_dialog(self):
        username_edit = Edit('Username: ')
        password_edit = Edit('Password: ')

        login_button = Button(
            'Login',
            lambda *args: self._do_login(username_edit.get_edit_text(), password_edit.get_edit_text()))

        cancel_button = Button(
            'Cancel',
            lambda *args: self._controller.exit())

        body = Pile([
            Divider(),
            username_edit,
            Divider(),
            password_edit,
            Divider(),
            Columns([(10, login_button), (10, cancel_button)])
        ])

        dialog = OverlayDialog(body, self._controller.game_widget, title='Login')

        self._controller.show(dialog)

    def _validate(self, username, password) -> bool:
        return self._username_regex.match(username) and len(password) > 0

    def _do_login(self, username, password):
        if not self._validate(username, password):
            widget = self._controller.widget
            close = Button('Close', on_press=lambda *args: self._controller.show(widget))
            dialog = TextDialog(self._controller.game_widget,
                                title='Validation error',
                                text='Illegal values',
                                buttons_line=Columns([(10, close)]))
            self._controller.show(dialog)
        else:
            dialog = TextDialog(self._controller.game_widget, text='Connecting...')
            self._controller.show(dialog)
            self._controller.send(LoginMessage(username, password))

    def on_message_user_info(self, message: UserInfoMessage):
        self._controller.player = UserInfo(
            message.name,
            UserStats(message.wins, message.draws, message.loses))

        StartGameBehavior(self._controller)


class ConnectionBehavior(Behavior):
    def __init__(self, controller: ClientGameController, host: str, port: int):
        super().__init__(controller)
        self._host = host
        self._port = port
        dialog = TextDialog(self._controller.game_widget, 'Connecting...')
        self._controller.show(dialog)

    async def connect(self):
        try:
            reader, writer = await asyncio.open_connection(self._host, self._port)
            self._controller.connection = ConnectionHandler(TextProtocol(), reader, writer, self)
            asyncio.create_task(self._controller.connection.start_receive_loop())
        except Exception:
            self._logger.error(f'Failed to connect to {self._host}:{self._port}', exc_info=True)
            dialog = TextDialog(
                self._controller.game_widget,
                title='Error',
                text='Connection error',
                buttons_line=Columns([(10, Button('Close', lambda *args: self._controller.exit()))]))
            self._controller.show(dialog)

    def on_message_hello(self, message: HelloMessage):
        self._logger.info(f'Hello received: {message}')
        LoginBehavior(self._controller)


_usage = """
usage:
   client <host> <port>
"""


def _main(argv):
    # TODO: use getopt; better help
    if len(argv) < 3:
        print(_usage)
        return

    try:
        host = argv[1]
        port = int(argv[2])
    except Exception:
        print(_usage)
        return

    main_widget = WidgetPlaceholder(SolidFill())

    def show_or_exit(key):
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()
        else:
            logger.debug(f'Unhandled key pressed: {key}')

    asyncio_event_loop = urwid.AsyncioEventLoop(loop=asyncio.get_event_loop())

    loop = urwid.MainLoop(
        main_widget,
        palette=palette,
        event_loop=asyncio_event_loop,
        unhandled_input=show_or_exit)

    game_controller = ClientGameController(main_widget, loop)
    behavior = ConnectionBehavior(game_controller, host, port)

    asyncio.get_event_loop().create_task(behavior.connect())
    loop.run()


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        filename='client.log',
        format='%(levelname)-5s %(asctime)s %(threadName)s %(name)s: %(message)s')
    _main(sys.argv)
