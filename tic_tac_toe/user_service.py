
from typing import Dict, Optional
from dataclasses import dataclass

import bcrypt
import random
import string

from tic_tac_toe import UserInfo, UserStats


__all__ = [
    'UserServiceException',
    'UserNotFoundError',
    'UserAlreadyExistsError',
    'UserService',
    'MemoryUserService'
]


class UserServiceException(Exception):
    pass


class UserNotFoundError(UserServiceException):
    pass


class UserAlreadyExistsError(UserServiceException):
    pass


class UserService:
    def find_by_credentials(self, name: str, password: str) -> Optional[UserInfo]:
        raise NotImplementedError

    def find_by_name(self, username: str) -> Optional[UserInfo]:
        raise NotImplementedError

    def create_user(self, name: str, password: str) -> UserInfo:
        raise NotImplementedError

    def save_game_result(self, name: str, wins: int = 0, draws: int = 0, loses: int = 0):
        raise NotImplementedError


def _random_salt():
    return ''.join(random.choice(string.printable) for i in range(16)).encode()


class MemoryUserService(UserService):
    @dataclass
    class PUser:
        name: str
        password_hash: bytes
        wins: int = 0
        draws: int = 0
        loses: int = 0

    def __init__(self, global_salt: bytes = _random_salt(), salt_rounds: int = 10):
        self._global_salt = global_salt
        self._salt_rounds = salt_rounds
        self._store: Dict[str, MemoryUserService.PUser] = {}

    def create_user(self, name: str, password: str) -> UserInfo:
        if name in self._store:
            raise UserAlreadyExistsError()
        user = self.PUser(
            name=name,
            password_hash=self._hashpw(name, password))
        self._store[name] = user
        return self._to_user_info(user)

    def find_by_credentials(self, name: str, password: str) -> Optional[UserInfo]:
        try:
            user = self._store[name]
            if user and self._checkpw(user, password):
                return self._to_user_info(user)
        except KeyError:
            return None

    def find_by_name(self, name: str) -> Optional[UserInfo]:
        try:
            return self._to_user_info(self._store[name])
        except KeyError:
            return None

    def save_game_result(self, name: str, wins: int = 0, draws: int = 0, loses: int = 0):
        assert wins >= 0
        assert draws >= 0
        assert loses >= 0

        if wins + draws + loses == 0:
            return

        try:
            user = self._store[name]
            user.wins += wins
            user.draws += draws
            user.loses += loses
        except KeyError:
            raise UserNotFoundError()

    def _hashpw(self, name: str, password: str) -> bytes:
        complex_pw = name.encode() + password.encode() + self._global_salt
        return bcrypt.hashpw(complex_pw, bcrypt.gensalt(self._salt_rounds))

    def _checkpw(self, user: PUser, password: str) -> bool:
        complex_pw = user.name.encode() + password.encode() + self._global_salt
        return bcrypt.checkpw(complex_pw, user.password_hash)

    def _to_user_info(self, user: PUser) -> UserInfo:
        return UserInfo(
            name=user.name,
            stats=UserStats(
                wins=user.wins,
                draws=user.draws,
                loses=user.loses))
