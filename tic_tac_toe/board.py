
import array

from typing import Optional, Tuple, Generator, Any

from tic_tac_toe import GameOverError, WrongTurnError, CellOccupiedError, Chip, GameResult


__all__ = [
    'Board'
]


class Board:
    edge_size = 3
    board_size = edge_size * edge_size

    def __init__(self):
        self.__result: Optional[GameResult] = None
        self.__next_chip: Optional[Chip] = Chip.X
        self.__board = array.array('b', [0 for _ in range(Board.board_size)])

    @property
    def next_chip(self) -> Optional[Chip]:
        return self.__next_chip

    @property
    def result(self) -> Optional[GameResult]:
        return self.__result

    @property
    def is_game_over(self) -> bool:
        return self.__result is not None

    def turn(self, cell: int, chip: Chip) -> Optional[GameResult]:
        assert 1 <= cell <= Board.board_size

        if not self.__next_chip:
            raise GameOverError("The game is over")
        elif chip != self.__next_chip:
            raise WrongTurnError(f"Wrong turn {self.__next_chip.name} expected")
        else:
            if self.__board[cell - 1] == 0:
                self.__board[cell - 1] = chip.value
                if self.__is_solved_by_position(cell, chip.value):
                    self.__result = GameResult(chip.value)
                    self.__next_chip = None
                    return self.__result
                elif 0 not in self.__board:
                    self.__result = GameResult.XO
                    self.__next_chip = None
                    return self.__result
                else:
                    self.__next_chip = Chip.O if chip == Chip.X else Chip.X
                    return None
            else:
                raise CellOccupiedError(f"Cell is occupied")

    def _abort(self, result: GameResult) -> GameResult:
        if not self.__result:
            self.__result = result
            self.__next_chip = None
        return self.__result

    def refuse(self, chip: Chip) -> GameResult:
        return self._abort(GameResult.X if chip == Chip.O else GameResult.O)

    def abort(self):
        return self._abort(GameResult.XO)

    def empty_cells(self) -> Generator[int, Any, None]:
        return (x + 1 for x in range(len(self.__board)) if self.__board[x] == 0)

    def __getitem__(self, cell: int) -> Optional[Chip]:
        return self.__get(cell)

    def __get(self, cell: int) -> Optional[Chip]:
        assert 1 <= cell <= self.board_size
        item = self.__board[cell - 1]
        if item == 0:
            return None
        else:
            return Chip(item)

    def __is_solved_by_position(self, cell, chip: int) -> bool:
        row = (cell - 1) // Board.edge_size + 1
        col = (cell - 1) % Board.edge_size + 1

        return (self.__is_line_solved(chip, self._column_cells(col))                           # check column
                or self.__is_line_solved(chip, self._row_cells(row))                           # check row
                or (row == col and self.__is_line_solved(chip, self._first_diagonal_cells()))  # check first diagonal
                or (row + col == Board.edge_size + 1                                           # check second diagonal
                    and self.__is_line_solved(chip, self._second_diagonal_cells())))

    def __is_line_solved(self, chip: int, line: range) -> bool:
        for x in line:
            if self.__board[x - 1] != chip:
                return False
        return True

    @staticmethod
    def _column_cells(column: int) -> range:
        assert 1 <= column <= Board.edge_size
        return range(column, Board.board_size + 1, Board.edge_size)

    @staticmethod
    def _row_cells(row: int) -> range:
        assert 1 <= row <= Board.edge_size
        r = row - 1
        return range(r * Board.edge_size + 1, r * Board.edge_size + Board.edge_size + 1)

    @staticmethod
    def _first_diagonal_cells() -> range:
        return range(1, Board.board_size + 1, Board.edge_size + 1)

    @staticmethod
    def _second_diagonal_cells() -> range:
        return range(Board.edge_size, Board.board_size, Board.edge_size - 1)

