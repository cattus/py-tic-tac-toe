
from typing import Optional

import asyncio
import random

from tic_tac_toe import GameResult, Chip, UserInfo
from tic_tac_toe.game import Game, PlayerController
from tic_tac_toe.board import Board

from tic_tac_toe.utils import LoggerMixin

__all__ = [
    'RandomAIPlayer',
    'BasicAIFactory'
]


class RandomAIPlayer(PlayerController, LoggerMixin):
    def __init__(self, name: str = 'RandomAIPlayer'):
        super().__init__()
        self._player_info = UserInfo(name)
        self._game: Optional[Game] = None
        self._board: Optional[Board] = None

    @property
    def player(self) -> UserInfo:
        return self._player_info

    async def _do_turn_async(self):
        cells = list(self._board.empty_cells())
        cell = cells[random.randint(0, len(cells) - 1)]
        self._logger.debug(f'{self._player_info.name}: do turn {cell}')
        self._game.turn(cell)
        self._board.turn(cell, self._game.chip)

    def _do_turn(self):
        asyncio.create_task(self._do_turn_async())

    def on_game_start(self, game: Game):
        assert self._game is None
        assert self._board is None

        self._logger.debug(f'{self._player_info.name}: on game start')

        self._game = game
        self._board = Board()
        if self._game.chip == Chip.X:
            self._do_turn()

    def on_opponent_turn(self, cell: int):
        if self._game:
            result = self._board.turn(cell, self._game.opponent_chip)
            self._logger.debug(f'{self._player_info.name}: on opponent turn {cell} -> {result}')
            if not result:
                self._do_turn()

    def on_game_over(self, result: GameResult):
        self._logger.debug(f'{self._player_info.name}: game over: {result}')
        self._game = None
        self._board = None

    def is_active(self):
        return True


class BasicAIFactory:
    def __init__(self):
        self._id = 0

    def create_player(self):
        self._id += 1
        return RandomAIPlayer(f'RandomAI-{self._id}')