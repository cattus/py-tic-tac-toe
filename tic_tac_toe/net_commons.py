
from typing import Optional

from asyncio import StreamReader, StreamWriter

from tic_tac_toe.utils import LoggerMixin

from tic_tac_toe.protocol import \
    Message, HelloMessage, LoginMessage, UserInfoMessage,\
    StartGameMessage, GameStartedMessage,\
    TurnMessage, GameOverMessage, ErrorMessage,\
    DecodingError, Protocol


__all__ = [
    'ConnectionHandler',
    'ConnectionListener'
]


class ConnectionListener:
    def on_unexpected_message(self, message: Message):
        pass

    def on_unexpected_error(self, exception: Exception):
        pass

    def on_decoding_error(self, exception: DecodingError):
        pass

    def on_disconnected(self):
        pass

    def on_message_hello(self, message: HelloMessage):
        self.on_unexpected_message(message)

    def on_message_login(self, message: LoginMessage):
        self.on_unexpected_message(message)

    def on_message_user_info(self, message: UserInfoMessage):
        self.on_unexpected_message(message)

    def on_message_start_game(self, message: StartGameMessage):
        self.on_unexpected_message(message)

    def on_message_game_started(self, message: GameStartedMessage):
        self.on_unexpected_message(message)

    def on_message_turn(self, message: TurnMessage):
        self.on_unexpected_message(message)

    def on_message_game_over(self, message: GameOverMessage):
        self.on_unexpected_message(message)

    def on_message_error(self, message: ErrorMessage):
        self.on_unexpected_message(message)


class ConnectionHandler(LoggerMixin):
    def __init__(self,
                 protocol: Protocol,
                 reader: StreamReader,
                 writer: StreamWriter,
                 listener: Optional[ConnectionListener] = None):
        super().__init__()
        self._reader = reader
        self._writer = writer
        self._protocol = protocol
        self._listener = listener
        self.__handlers = {
            HelloMessage: self._on_message_hello,
            LoginMessage: self._on_message_login,
            UserInfoMessage: self._on_message_user_info,
            StartGameMessage: self._on_message_start_game,
            GameStartedMessage: self._on_message_game_started,
            TurnMessage: self._on_message_turn,
            GameOverMessage: self._on_message_game_over,
            ErrorMessage: self._on_message_error
        }

    @property
    def listener(self) -> Optional[ConnectionListener]:
        return self._listener

    @listener.setter
    def listener(self, listener: Optional[ConnectionListener]):
        self._listener = listener

    def send(self, message: Message):
        assert self._writer, 'not initialized'

        try:
            encoded = self._protocol.encode(message) + b'\r\n'
            self._writer.write(encoded)
            self._logger.debug(f'Data sent: {encoded}')
        except Exception as e:
            self._logger.error(f'Failed to send message {message}: {e}')
            self.close()

    async def receive(self) -> Optional[Message]:
        assert self._reader

        while True:
            line = await self._reader.readline()
            if not line:
                return None
            elif len(line.strip()) > 0:
                return self._protocol.decode(line)
            else:
                continue

    def close(self):
        if self._writer:
            self._writer.close()

    async def start_receive_loop(self):
        assert self._reader and self._writer, 'not initialized'

        try:
            while True and not self._writer.is_closing():
                message: Optional[Message] = None
                try:
                    message = await self.receive()
                    if not message:
                        self._logger.info('Disconnected')
                        break
                except DecodingError as e:
                    self._logger.info(f'DecodingError: {e}')
                    self._on_decoding_error(e)
                except Exception as e:
                    self._logger.error(f'Unexpected error while decoding', exc_info=True)
                    self._on_unexpected_error(e)

                if message:
                    try:
                        self._on_message(message)
                    except Exception as e:
                        self._logger.error(f'Unexpected error on message processing', exc_info=True)
                        self._on_unexpected_error(e)
        finally:
            self._on_disconnected()

    def _on_unexpected_error(self, exception: Exception):
        try:
            if self._listener:
                self._listener.on_unexpected_error(exception)
        except Exception:
            self._logger.error(f'Error terminating', exc_info=True)
            self.close()

    def _on_decoding_error(self, exception: DecodingError):
        if self._listener:
            self._listener.on_decoding_error(exception)

    def _on_disconnected(self):
        if self._listener:
            self._listener.on_disconnected()

    def _on_message(self, message: Message):
        handler = self.__handlers[message.__class__]
        handler(message)

    def _on_message_hello(self, message: HelloMessage):
        if self._listener:
            self._listener.on_message_hello(message)

    def _on_message_login(self, message: LoginMessage):
        if self._listener:
            self._listener.on_message_login(message)

    def _on_message_user_info(self, message: UserInfoMessage):
        if self._listener:
            self._listener.on_message_user_info(message)

    def _on_message_start_game(self, message: StartGameMessage):
        if self._listener:
            self._listener.on_message_start_game(message)

    def _on_message_game_started(self, message: GameStartedMessage):
        if self._listener:
            self._listener.on_message_game_started(message)

    def _on_message_turn(self, message: TurnMessage):
        if self._listener:
            self._listener.on_message_turn(message)

    def _on_message_game_over(self, message: GameOverMessage):
        if self._listener:
            self._listener.on_message_game_over(message)

    def _on_message_error(self, message: ErrorMessage):
        if self._listener:
            self._listener.on_message_error(message)

