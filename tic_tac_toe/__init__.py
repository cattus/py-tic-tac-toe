
from dataclasses import dataclass

from enum import Enum
import enum


__version__ = '0.0.1'

__all__ = [
    'GameException',
    'WrongTurnError',
    'GameOverError',
    'CellOccupiedError',
    'GameResult',
    'Chip',
    'UserStats',
    'UserInfo'
]


class GameException(Exception):
    pass


class WrongTurnError(GameException):
    pass


class GameOverError(GameException):
    pass


class CellOccupiedError(GameException):
    pass


@enum.unique
class GameResult(Enum):
    X = 1
    O = 2
    XO = 3


@enum.unique
class Chip(Enum):
    X = 1
    O = 2


@dataclass
class UserStats:
    wins: int = 0
    draws: int = 0
    loses: int = 0


@dataclass
class UserInfo:
    name: str
    stats: UserStats = UserStats()
