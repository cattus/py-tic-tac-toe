
from typing import Optional

from asyncio import StreamReader, StreamWriter

import asyncio

import logging

from tic_tac_toe import \
    WrongTurnError, CellOccupiedError, GameOverError, \
    UserInfo, GameResult, Chip

from tic_tac_toe.board import Board

from tic_tac_toe.ai import BasicAIFactory
from tic_tac_toe.net_commons import ConnectionHandler
from tic_tac_toe.game import GameLauncher, Game, PlayerController
from tic_tac_toe.user_service import UserService, MemoryUserService, UserAlreadyExistsError

from tic_tac_toe.protocol import \
    Message, HelloMessage, LoginMessage, UserInfoMessage, \
    StartGameMessage, GameStartedMessage, \
    TurnMessage, GameOverMessage, ErrorMessage, \
    DecodingError, Protocol, TextProtocol

__all__ = [
    'ServerConnectionHandler',
    'AsyncIOServer'
]


class ServerConnectionHandler(ConnectionHandler, PlayerController):
    def __init__(self,
                 protocol: Protocol,
                 reader: StreamReader,
                 writer: StreamWriter,
                 game_launcher: GameLauncher,
                 user_service: UserService):
        super().__init__(protocol, reader, writer)
        self._user_service = user_service
        self._game_launcher = game_launcher
        self._user_info: Optional[UserInfo] = None
        self._in_game = False
        self._game: Optional[Game] = None

    async def start(self):
        self.send(HelloMessage(1, 'Welcome to tic tac toe server'))
        await self.start_receive_loop()

    @property
    def player(self) -> UserInfo:
        if self._user_info:
            return self._user_info
        else:
            raise LookupError('PlayerInfo not set')  # unexpected condition

    @property
    def game(self) -> Optional[Game]:
        return self._game

    def on_game_start(self, game: Game):
        self._logger.debug('Game started')
        self._game = game
        self.send(GameStartedMessage(game.x_player.name, game.o_player.name))
        self._logger.debug('Game started: start sent')

    def on_opponent_turn(self, cell: int):
        self.send(TurnMessage(cell))

    def on_game_over(self, result: GameResult):
        chip = self._game.chip
        name = self._user_info.name
        wins = 0
        draws = 0
        loses = 0

        if result == GameResult.XO:
            draws += 1
        elif (chip == Chip.X and result == GameResult.X) or (chip == Chip.O and result == GameResult.O):
            wins += 1
        else:
            loses += 1

        self._user_info.stats.wins += wins
        self._user_info.stats.loses += loses
        self._user_info.stats.draws += draws

        self._user_service.save_game_result(name, wins=wins, draws=draws, loses=loses)

        self._in_game = False
        self._game = None
        self.send(GameOverMessage(result))
        self._send_user_info()

    def is_active(self):
        return not self._writer.transport.is_closing()

    def _on_unexpected_error(self, exception: Exception):
        self.send(ErrorMessage("unexpected-error"))
        if self._game:
            self._game.abort()
        self.close()

    def _on_disconnected(self):
        if self._game:
            self._game.refuse()

    def _on_decoding_error(self, exception: DecodingError):
        self.send(ErrorMessage('invalid-message'))

    def _on_message_unknown(self, message: Message):
        self._logger.warning(f'unknown message: {message}')
        self.send(ErrorMessage('unknown-message'))

    def _on_message_login(self, message: LoginMessage):
        if self._user_info:
            self.send(ErrorMessage('already-logged-in'))
        else:
            try:
                self._user_info = self._user_service.find_by_credentials(message.name, message.password)
                if not self._user_info:
                    self._user_info = self._user_service.create_user(message.name, message.password)
                self._send_user_info()
            except UserAlreadyExistsError:
                self.send(ErrorMessage('wrong-password'))

    def _on_message_hello(self, message: HelloMessage):
        self._on_message_unknown(message)

    def _on_message_user_info(self, message: UserInfoMessage):
        self._on_message_unknown(message)

    def _on_message_start_game(self, message: StartGameMessage):
        if not self._user_info:
            self.send(ErrorMessage('not-logged-in'))
        elif self._in_game:
            self.send(ErrorMessage('already-started'))
        else:
            self._in_game = True
            asyncio.create_task(self._game_launcher.enqueue(self))

    def _on_message_game_started(self, message: GameStartedMessage):
        self._on_message_unknown(message)

    def _on_message_turn(self, message: TurnMessage):
        if 1 <= message.cell <= Board.board_size:
            if self._game:
                try:
                    self._game.turn(message.cell)
                except WrongTurnError:
                    self.send(ErrorMessage(f'wrong-turn'))
                except CellOccupiedError:
                    self.send(ErrorMessage(f'cell-occupied'))
                except GameOverError:
                    self.send(ErrorMessage(f'game-over'))
            else:
                self.send(ErrorMessage(f'game-not-started'))
        else:
            self.send(ErrorMessage('out-of-range'))

    def _on_message_game_over(self, message: GameOverMessage):
        self._on_message_unknown(message)

    def _on_message_error(self, message: ErrorMessage):
        self._on_message_unknown(message)

    def _send_user_info(self):
        stats = self._user_info.stats
        self.send(UserInfoMessage(
            name=self._user_info.name,
            wins=stats.wins,
            draws=stats.draws,
            loses=stats.loses))


class AsyncIOServer:
    def __init__(self, host: str, port: int, game_launcher: GameLauncher, user_service: UserService):
        self._game_launcher = game_launcher
        self._user_service = user_service
        self._logger = logging.getLogger(f'{self.__module__}.{self.__class__.__name__}')
        self._host = host
        self._port = port
        self._server: Optional[asyncio.AbstractServer] = None
        self._server_future = None
        self._protocol = TextProtocol()

    async def _connection_worker(self, reader: StreamReader, writer: StreamWriter):
        try:
            handler = ServerConnectionHandler(
                protocol=self._protocol,
                reader=reader,
                writer=writer,
                game_launcher=self._game_launcher,
                user_service=self._user_service)
            await handler.start()
        except AttributeError as e:
            self._logger.error(f'Error creating handler {e}')
            writer.close()

    async def start(self):
        self._logger.info('Starting TCP server')
        self._server = await asyncio.start_server(
            self._connection_worker,
            self._host,
            self._port)
        self._logger.debug(f'Server started: {self._server}')
        await self._server.serve_forever()
        self._logger.info('TCP server done')


_usage = """
usage:
    server <host> <port>
"""


async def _main(argv):
    # TODO: use getopt; better help
    if len(argv) < 3:
        print(_usage)
        return

    try:
        host = argv[1]
        port = int(argv[2])
    except Exception:
        print(_usage)
        return

    ai_factory = BasicAIFactory()
    launcher = GameLauncher(ai_recovery_timeout=5,
                            ai_player_factory=ai_factory.create_player)
    server = AsyncIOServer(
        host=host,
        port=port,
        game_launcher=launcher,
        user_service=MemoryUserService()
    )
    launcher_task = asyncio.create_task(launcher.start())
    await server.start()
    await launcher_task


if __name__ == "__main__":
    import sys
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)-5s %(asctime)s %(threadName)s %(name)s: %(message)s')
    asyncio.run(_main(sys.argv))
