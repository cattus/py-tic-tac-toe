
from typing import Optional

import random

from concurrent.futures import CancelledError
from concurrent import futures

import asyncio
from asyncio.tasks import Task

from tic_tac_toe import UserInfo, Chip, GameResult
from tic_tac_toe.board import Board
from tic_tac_toe.utils import LoggerMixin


__all__ = [
    'GameControllerInterface',
    'Game',
    'PlayerController',
    'GameController',
    'GameLauncher'
]


class GameControllerInterface:
    def turn(self, cell: int, chip: Chip):
        raise NotImplementedError

    def refuse(self, chip: Chip):
        raise NotImplementedError

    def abort(self):
        raise NotImplementedError

    @property
    def x_player(self) -> UserInfo:
        raise NotImplementedError

    @property
    def o_player(self) -> UserInfo:
        raise NotImplementedError


class Game:
    def __init__(self, game_controller: GameControllerInterface, chip: Chip):
        self._chip = chip
        self._game_controller = game_controller

    @property
    def x_player(self) -> UserInfo:
        return self._game_controller.x_player

    @property
    def o_player(self) -> UserInfo:
        return self._game_controller.o_player

    @property
    def opponent(self) -> UserInfo:
        return self.o_player if self.chip == Chip.X else self.x_player

    @property
    def chip(self) -> Chip:
        return self._chip

    @property
    def opponent_chip(self) -> Chip:
        return Chip.X if self.chip == Chip.O else Chip.O

    def turn(self, cell: int):
        self._game_controller.turn(cell, self.chip)

    def refuse(self):
        self._game_controller.refuse(self._chip)

    def abort(self):
        self._game_controller.abort()


class PlayerController:
    @property
    def player(self) -> UserInfo:
        raise NotImplementedError

    def on_game_start(self, game: Game):
        raise NotImplementedError

    def on_opponent_turn(self, cell: int):
        raise NotImplementedError

    def on_game_over(self, result: GameResult):
        raise NotImplementedError

    def is_active(self) -> bool:
        raise NotImplementedError


class GameController(GameControllerInterface):
    def __init__(self, x_player: PlayerController, o_player: PlayerController):
        super().__init__()
        self._x_player = x_player
        self._o_player = o_player
        self._board = Board()

    def start(self):
        self._o_player.on_game_start(Game(self, Chip.O))
        self._x_player.on_game_start(Game(self, Chip.X))

    @property
    def x_player(self) -> UserInfo:
        return self._x_player.player

    @property
    def o_player(self) -> UserInfo:
        return self._o_player.player

    def turn(self, cell: int, chip: Chip):
        result = self._board.turn(cell, chip)

        if chip == Chip.X:
            self._o_player.on_opponent_turn(cell)
        else:
            self._x_player.on_opponent_turn(cell)

        if result:
            self._emit_result(result)

    def refuse(self, chip: Chip):
        if not self._board.result:
            result = self._board.refuse(chip)
            self._emit_result(result)

    def abort(self):
        if not self._board.result:
            result = self._board.abort()
            self._emit_result(result)

    def _emit_result(self, result: GameResult):
        self._x_player.on_game_over(result)
        self._o_player.on_game_over(result)


class GameLauncher(LoggerMixin):
    def __init__(
            self,
            ai_recovery_timeout: float = 10.0,
            ai_player_factory=None):
        super().__init__()
        self._ai_recovery_timeout = ai_recovery_timeout
        self._ai_player_factory = ai_player_factory
        self._queue: asyncio.Queue[PlayerController] = asyncio.Queue()
        self._loop_future: Optional[Task] = None

    async def enqueue(self, player: PlayerController):
        self._logger.debug(f'Enqueuing player: {player.player.name}')
        await self._queue.put(player)

    async def _dequeue(self) -> PlayerController:
        while True:
            p = await self._queue.get()
            if p.is_active():
                return p

    async def _dequeue_or_ai(self):
        if self._ai_player_factory:
            try:
                return await asyncio.wait_for(self._dequeue(), self._ai_recovery_timeout)
            except futures.TimeoutError:
                return self._ai_player_factory()
        else:
            return await self._dequeue()

    async def _start_game(self):
        players = (await self._dequeue(), await self._dequeue_or_ai())
        r = random.randint(0, 1)
        x_player = players[r]
        o_player = players[0 if r == 1 else 1]
        game_controller = GameController(x_player, o_player)
        game_controller.start()
        self._logger.debug(f'Game started for: {x_player.player} {o_player.player}')

    async def _start(self):
        self._logger.debug('Starting GameLauncher')
        while True:
            await self._start_game()

    async def start(self):
        if not self._loop_future:
            self._loop_future = asyncio.create_task(self._start())
        await self.wait_stopped()
        self._loop_future = None

    def stop(self):
        if self._loop_future:  # pragma: no cover
            self._loop_future.cancel()

    async def wait_stopped(self):
        if self._loop_future:  # pragma: no cover
            try:
                await self._loop_future
            except CancelledError:
                pass
