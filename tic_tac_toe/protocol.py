
from dataclasses import dataclass

from typing import Dict, Type, Callable
import re

from tic_tac_toe import GameResult


__all__ = [
    'Message',
    'HelloMessage',
    'LoginMessage',
    'UserInfoMessage',
    'StartGameMessage',
    'GameStartedMessage',
    'TurnMessage',
    'GameOverMessage',
    'ErrorMessage',
    'ProtocolError',
    'EncodingError',
    'DecodingError',
    'Encoder',
    'Decoder',
    'Protocol',
    'TextEncoder',
    'TextDecoder',
    'TextProtocol'
]


@dataclass(frozen=True)
class Message:
    pass


@dataclass(frozen=True)
class HelloMessage(Message):
    version: int
    server_info: str


@dataclass(frozen=True)
class LoginMessage(Message):
    name: str
    password: str


@dataclass(frozen=True)
class UserInfoMessage(Message):
    name: str
    wins: int = 0
    draws: int = 0
    loses: int = 0


@dataclass(frozen=True)
class StartGameMessage(Message):
    pass


@dataclass(frozen=True)
class GameStartedMessage(Message):
    x_player_name: str
    o_player_name: str


@dataclass(frozen=True)
class TurnMessage(Message):
    cell: int = 0


@dataclass(frozen=True)
class GameOverMessage(Message):
    result: GameResult


@dataclass(frozen=True)
class ErrorMessage(Message):
    message: str


class ProtocolError(Exception):
    pass


class EncodingError(ProtocolError):
    pass


class DecodingError(ProtocolError):
    pass


class Encoder:
    def encode(self, message: Message) -> bytes:
        raise NotImplementedError


class Decoder:
    def decode(self, data: bytes) -> Message:
        raise NotImplementedError


class Protocol(Encoder, Decoder):
    def encode(self, message: Message) -> bytes:
        raise NotImplementedError

    def decode(self, data: bytes) -> Message:
        raise NotImplementedError


class TextEncoder(Encoder):
    def __init__(self):
        super().__init__()
        e = TextEncoder
        # noinspection PyTypeChecker
        self.__encoders: Dict[Type[Message], Callable[[Message], str]] = {
          HelloMessage: e._encode_hello,
          LoginMessage: e._encode_login,
          UserInfoMessage: e._encode_user_info,
          StartGameMessage: e._encode_start_game,
          GameStartedMessage: e._encode_game_started,
          TurnMessage: e._encode_turn,
          GameOverMessage: e._encode_game_over,
          ErrorMessage: e._encode_error_message
        }

    def encode(self, message: Message) -> bytes:
        return self.__encoders[message.__class__](message).encode()

    @staticmethod
    def _encode_hello(m: HelloMessage) -> str:
        return f'hello {m.version} {m.server_info}'

    @staticmethod
    def _encode_login(m: LoginMessage) -> str:
        return f'login {m.name} {m.password}'

    @staticmethod
    def _encode_user_info(m: UserInfoMessage) -> str:
        return f'user {m.name} {m.wins} {m.draws} {m.loses}'

    @staticmethod
    def _encode_start_game(m: StartGameMessage) -> str:
        return f'start'

    @staticmethod
    def _encode_game_started(m: GameStartedMessage) -> str:
        return f'game {m.x_player_name} {m.o_player_name}'

    @staticmethod
    def _encode_turn(m: TurnMessage) -> str:
        return f'turn {m.cell}'

    @staticmethod
    def _encode_game_over(m: GameOverMessage) -> str:
        return f'over {m.result.name}'

    @staticmethod
    def _encode_error_message(m: ErrorMessage) -> str:
        return f'error {m.message}'


class TextDecoder(Decoder):
    __re_hello = re.compile(r'hello\s+'
                            r'(?P<version>\d+)\s+'
                            r'(?P<server_info>.+)$',
                            re.I)

    __re_login = re.compile(r'login\s+'
                            r'(?P<username>\S+)\s+'
                            r'(?P<password>\S+)$',
                            re.I)

    __re_user_info = re.compile(r'user\s+'
                                r'(?P<username>\S+)\s+'
                                r'(?P<wins_count>\d+)\s+'
                                r'(?P<draws_count>\d+)\s+'
                                r'(?P<loses_count>\d+)$',
                                re.I)

    __re_start_game = re.compile(r'start', re.I)

    __re_game_started = re.compile(r'game\s+'
                                   r'(?P<x_player_name>\S+)\s+'
                                   r'(?P<o_player_name>\S+)\s*$',
                                   re.I)

    __re_turn = re.compile(r'turn\s+'
                           r'(?P<cell>\d+)$',
                           re.I)

    __re_game_over = re.compile(r'over\s+'
                                r'(?P<result>\S+)\s*$',
                                re.I)

    __re_error_message = re.compile(r'error\s+'
                                    r'(?P<message>.+)$',
                                    re.I)

    def __init__(self):
        super().__init__()
        d = TextDecoder
        self.__decoders = [
            (d.__re_hello, d._decode_hello),
            (d.__re_login, d._decode_login),
            (d.__re_user_info, d._decode_user_info),
            (d.__re_start_game, d._decode_start_game),
            (d.__re_game_started, d._decode_game_started),
            (d.__re_turn, d._decode_turn),
            (d.__re_game_over, d._decode_game_over),
            (d.__re_error_message, d._decode_error_message)
        ]

    def decode(self, data: bytes) -> Message:
        line = data.decode().strip()
        for pattern, handler in self.__decoders:
            matched = pattern.match(line)
            if matched:
                try:
                    return handler(matched.groupdict())
                except (LookupError, AssertionError, ValueError) as e:
                    raise DecodingError('Failed to decode message', line, e)
        raise DecodingError('Illegal command', line)

    @staticmethod
    def _decode_hello(data: dict) -> HelloMessage:
        return HelloMessage(version=int(data['version']),
                            server_info=data['server_info'])

    @staticmethod
    def _decode_login(data: dict) -> LoginMessage:
        return LoginMessage(name=data['username'],
                            password=data['password'])

    @staticmethod
    def _decode_user_info(data: dict) -> UserInfoMessage:
        return UserInfoMessage(name=data['username'],
                               wins=int(data['wins_count']),
                               draws=int(data['draws_count']),
                               loses=int(data['loses_count']))

    @staticmethod
    def _decode_start_game(data: dict) -> StartGameMessage:
        return StartGameMessage()

    @staticmethod
    def _decode_game_started(data: dict) -> GameStartedMessage:
        return GameStartedMessage(x_player_name=data['x_player_name'],
                                  o_player_name=data['o_player_name'])

    @staticmethod
    def _decode_turn(data: dict) -> TurnMessage:
        return TurnMessage(cell=int(data['cell']))

    @staticmethod
    def _decode_game_over(data: dict) -> GameOverMessage:
        return GameOverMessage(GameResult[data['result']])

    @staticmethod
    def _decode_error_message(data: dict) -> ErrorMessage:
        return ErrorMessage(data['message'])


class TextProtocol(TextDecoder, TextEncoder, Protocol):
    def __init__(self):
        super().__init__()
