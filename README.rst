===========
tic-tac-toe
===========

.. contents:: **Table of Contents**
    :backlinks: none

Running in dev mode
===================

Preparing dev environment
-------------------------

All commands should run in project directory.

Initialize virtual environment:

.. code-block::

   $ python3 -m venv .venv
   $ source .venv/bin/activate
   $ export PYTHONPATH="$PYTHONPATH:$PWD"

Install dependencies:

.. code-block::

   $ pip install -r requirements.txt


Starting server
---------------

.. code-block::

  $ python tic_tac_toe/server.py localhost 9999



Running in docker container
===========================

.. code-block::

   $ docker build -t py_tic_tac_toe .
   $ docker run 9999:9999 py_tic_tac_toe



Playing the game
================

Basic TUI client implemented with the Urwid library http://urwid.org/.

.. code-block::

   $ python tic_tac_toe/client.py localhost 9999


Server protocol
===============

The basic session:

.. code-block::

   $ telnet localhost 9999
   Trying 127.0.0.1...
   Connected to localhost.
   Escape character is '^]'.
   hello 1 Welcome to tic tac toe server
   login buzz buzzpass
   start
   game buzz RandomAI-3
   turn 9
   turn 6
   turn 1
   turn 3
   turn 7
   turn 8
   turn 3
   turn 5
   over X
   user u 2 1 0


After connection the server sends the welcome message:

.. code-block::

   server> hello 1 Welcome to tic tac toe server

Then the user should login in the system.
If the usr does not exist the new user will be created.

.. code-block::

   client> login username password

The server will respond with user statistics:

.. code-block::

   server> user username 2 1 0

Here 2 is the count of wins,
1 is the total number of draws,
and 0 is the total number of loses.


After the login the client can start the game.

.. code-block::

   client> start

The server will try to find another player.
Or if there is no player will start the game with AI Bot.


.. code-block::

   server> game buzz RandomAI-3

The first player (buzz) in above message will play X and the second (RandomAI-3) will play 0.

.. code-block::

   client> turn 9
   server> turn 6
   client> turn 1

After the game is started. Peers can do their turns.
Marking the cells with their chips by the cell indexes (1..9)
starting from top left corner.

+---+---+---+
| 1 | 2 | 3 |
+---+---+---+
| 4 | 5 | 6 |
+---+---+---+
| 7 | 8 | 9 |
+---+---+---+

When the game is over.
The server will respond with the game result and updated user's statistics.

.. code-block::

   server> over X
   server> user username 2 1 0

After the game is over. The user can start the new game.

.. code-block::

   client> start


Installation
------------

tic-tac-toe is distributed on `PyPI <https://pypi.org>`_ as a universal
wheel and is available on Linux/macOS and Windows and supports
Python 2.7/3.5+ and PyPy.

.. code-block:: bash

    $ pip install tic-tac-toe

License
-------

tic-tac-toe is distributed under the terms of both

- `MIT License <https://choosealicense.com/licenses/mit>`_
- `Apache License, Version 2.0 <https://choosealicense.com/licenses/apache-2.0>`_

at your option.
