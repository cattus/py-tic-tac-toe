import pytest

from asyncio import wait_for
import asyncio

from typing import Tuple
from unittest.mock import MagicMock, PropertyMock

from tic_tac_toe import Chip, GameResult, UserInfo

from tic_tac_toe.game import \
    Game, GameControllerInterface, GameController, GameLauncher, PlayerController


def _new_player_controller(name) -> PlayerController:
    p = PlayerController()
    type(p).player = PropertyMock(return_value=UserInfo(name))
    p.is_active = MagicMock(return_value=True)
    p.on_game_over = MagicMock()
    p.on_opponent_turn = MagicMock()
    p.on_game_start = MagicMock()
    return p


def _new_game_controller_with_games() -> Tuple[GameControllerInterface, Game, Game]:
    c = GameControllerInterface()
    type(c).x_player = PropertyMock(return_value=UserInfo('X'))
    type(c).o_player = PropertyMock(return_value=UserInfo('O'))
    xg = Game(c, Chip.X)
    og = Game(c, Chip.O)
    return c, xg, og


def _solve_the_game(c: GameControllerInterface):
    c.turn(1, Chip.X)
    c.turn(4, Chip.O)
    c.turn(5, Chip.X)
    c.turn(7, Chip.O)
    c.turn(9, Chip.X)


class TestGame:

    def test_should_return_players(self):
        c, xg, og = _new_game_controller_with_games()
        assert xg.x_player == c.x_player
        assert og.x_player == c.x_player
        assert xg.o_player == c.o_player
        assert og.o_player == c.o_player

    def test_should_return_opponent_player(self):
        c, xg, og = _new_game_controller_with_games()
        assert xg.opponent == c.o_player
        assert og.opponent == c.x_player
        assert xg.opponent_chip == Chip.O
        assert og.opponent_chip == Chip.X

    def test_should_pass_the_turn(self):
        c, xg, og = _new_game_controller_with_games()
        c.turn = MagicMock()
        xg.turn(2)
        c.turn.assert_called_once_with(2, Chip.X)
        c.turn.reset_mock()
        og.turn(8)
        c.turn.assert_called_once_with(8, Chip.O)

    def test_should_pass_abort(self):
        c, xg, og = _new_game_controller_with_games()
        c.abort = MagicMock()
        xg.abort()
        c.abort.assert_called_once()

    def test_should_pass_refuse(self):
        c, xg, og = _new_game_controller_with_games()
        c.refuse = MagicMock()
        xg.refuse()
        c.refuse.assert_called_once_with(Chip.X)
        c.refuse.reset_mock()
        og.refuse()
        c.refuse.assert_called_once_with(Chip.O)


class TestGameController:
    def test_should_return_players_info(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)
        assert c.x_player == xp.player
        assert c.o_player == op.player

    def test_should_start_game(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')

        c = GameController(xp, op)
        c.start()

        xp.on_game_start.assert_called_once()
        xp_game = xp.on_game_start.call_args[0][0]
        assert isinstance(xp_game, Game)
        assert xp_game.chip == Chip.X

        op.on_game_start.assert_called_once()
        op_game = op.on_game_start.call_args[0][0]
        assert isinstance(op_game, Game)
        assert op_game.chip == Chip.O

    def test_should_pass_the_turn(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)

        c.turn(4, Chip.X)
        op.on_opponent_turn.assert_called_once_with(4)
        xp.on_opponent_turn.assert_not_called()

        op.on_opponent_turn.reset_mock()
        xp.on_opponent_turn.reset_mock()

        c.turn(2, Chip.O)
        xp.on_opponent_turn.assert_called_once_with(2)
        op.on_opponent_turn.assert_not_called()

    def test_should_pass_game_over_on_game_over(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)
        _solve_the_game(c)

        op.on_game_over.assert_called_once()
        xp.on_game_over.assert_called_once()
        assert op.on_game_over.call_args[0][0] == GameResult.X
        assert xp.on_game_over.call_args[0][0] == GameResult.X

    def test_game_over_on_abort(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)
        c.abort()
        assert op.on_game_over.call_args[0][0] == GameResult.XO
        assert xp.on_game_over.call_args[0][0] == GameResult.XO

    def test_do_not_call_game_over_on_abort_when_game_is_over(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)
        _solve_the_game(c)
        xp.on_game_over.reset_mock()
        op.on_game_over.reset_mock()
        c.abort()
        xp.on_game_over.assert_not_called()
        op.on_game_over.assert_not_called()

    def test_game_over_on_refuse(self):
        def check(chip: Chip, result: GameResult):
            xp = _new_player_controller('X')
            op = _new_player_controller('O')
            c = GameController(xp, op)
            c.refuse(chip)
            assert op.on_game_over.call_args[0][0] == result
            assert xp.on_game_over.call_args[0][0] == result

        check(Chip.X, GameResult.O)
        check(Chip.O, GameResult.X)

    def test_do_not_call_game_over_on_refuse_when_game_is_over(self):
        xp = _new_player_controller('X')
        op = _new_player_controller('O')
        c = GameController(xp, op)
        _solve_the_game(c)
        xp.on_game_over.reset_mock()
        op.on_game_over.reset_mock()
        c.refuse(Chip.X)
        xp.on_game_over.assert_not_called()
        op.on_game_over.assert_not_called()


class TestGameLauncher:
    @pytest.mark.asyncio
    async def test_should_enqueue_player(self):
        l = GameLauncher()
        p = _new_player_controller('X')
        p.is_active = MagicMock(return_value=True)
        await wait_for(l.enqueue(p), timeout=1)
        found = await wait_for(l._dequeue(), timeout=1)
        assert found == p

    @pytest.mark.asyncio
    async def test_should_start_game_if_players_available(self):
        p1_start_event = asyncio.Event()
        p2_start_event = asyncio.Event()

        def p1_game_started(game):
            p1_start_event.set()

        def p2_game_started(game):
            p2_start_event.set()

        l = GameLauncher()
        p1 = _new_player_controller('p1')
        p2 = _new_player_controller('p2')

        p1.on_game_start = MagicMock(side_effect=p1_game_started)
        p2.on_game_start = MagicMock(side_effect=p2_game_started)

        asyncio.create_task(l.start())

        await wait_for(l.enqueue(p1), 1)
        await wait_for(l.enqueue(p2), 1)

        await wait_for(p1_start_event.wait(), 1)
        await wait_for(p2_start_event.wait(), 1)

        l.stop()
        await l.wait_stopped()

        p1.on_game_start.assert_called_once()
        p2.on_game_start.assert_called_once()

    @pytest.mark.asyncio
    async def test_should_skip_inactive_players(self):
        p1_start_event = asyncio.Event()
        p2_start_event = asyncio.Event()

        def p1_game_started(game):
            p1_start_event.set()

        def p2_game_started(game):
            p2_start_event.set()

        l = GameLauncher()
        p1 = _new_player_controller('p1')
        p2 = _new_player_controller('p2')
        p3 = _new_player_controller('p3-inactive')
        p3.is_active = MagicMock(return_value=False)

        p1.on_game_start = MagicMock(side_effect=p1_game_started)
        p2.on_game_start = MagicMock(side_effect=p2_game_started)

        asyncio.create_task(l.start())
        asyncio.create_task(l.start())

        await wait_for(l.enqueue(p1), 1)
        await wait_for(l.enqueue(p3), 1)
        await wait_for(l.enqueue(p2), 1)

        await wait_for(p1_start_event.wait(), 1)
        await wait_for(p2_start_event.wait(), 1)

        l.stop()
        await l.wait_stopped()

        p1.on_game_start.assert_called_once()
        p2.on_game_start.assert_called_once()

    @pytest.mark.asyncio
    async def test_should_dequeue_player_or_return_ai(self):
        p1_start_event = asyncio.Event()

        def p1_game_started(game):
            p1_start_event.set()

        def ai_factory():
            return _new_player_controller('ai')

        l = GameLauncher(ai_recovery_timeout=1/1000, ai_player_factory=ai_factory)
        p1 = _new_player_controller('p1')

        p1.on_game_start = MagicMock(side_effect=p1_game_started)

        asyncio.create_task(l.start())

        await wait_for(l.enqueue(p1), 1)

        await wait_for(p1_start_event.wait(), 3)
        p1.on_game_start.assert_called_once()
        game = p1.on_game_start.call_args[0][0]
        assert game.opponent.name == 'ai'

        l.stop()
        await l.wait_stopped()
