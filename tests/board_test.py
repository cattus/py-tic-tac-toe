from typing import List, Tuple, Optional, Union
import pytest

from tic_tac_toe import \
    Chip, GameResult,\
    WrongTurnError, CellOccupiedError, GameOverError

from tic_tac_toe.board import Board


def solve_for_player(winner: Chip):
    for c in range(1, Board.edge_size):
        solve_by_line(Board._column_cells(c), winner)

    for r in range(1, Board.edge_size):
        solve_by_line(Board._row_cells(r), winner)

    solve_by_line(Board._first_diagonal_cells(), winner)
    solve_by_line(Board._second_diagonal_cells(), winner)


def solve_by_line(line: Union[range, List[int]], winner: Chip, board: Optional[Board] = None):
    if isinstance(line, range):
        line = list(line)

    if board is None:
        board = Board()

    line_iter = iter(line)
    turn = 0
    while not board.is_game_over:
        if board.next_chip == winner:
            c = next(line_iter)
            board.turn(c, board.next_chip)
        else:
            empty_cells = list(board.empty_cells())
            if winner == Chip.O and turn % 3 != 0:  # avoid good moves for O player
                empty_cells = reversed(empty_cells)
            c = next(c for c in empty_cells if c not in line)
            board.turn(c, board.next_chip)
        turn += 1
    assert board.result == GameResult(winner.value)
    assert board.is_game_over is True
    assert board.next_chip is None


def test_board_should_be_properly_created():
    board = Board()
    assert board.next_chip == Chip.X
    assert board.result is None
    assert board.is_game_over is False


def test_turn_should_not_accept_invalid_cells():
    for c in [0, -1, Board.board_size + 1]:
        board = Board()
        with pytest.raises(AssertionError):
            board.turn(c, Chip.X)


def test_turn_should_accept_valid_coords():
    for c in range(1, Board.board_size):
        board = Board()
        assert board[c] is None
        board.turn(c, Chip.X)
        assert board[c] == Chip.X


def test_turn_should_properly_set_next_chip():
    board = Board()
    board.turn(1, Chip.X)
    assert board.next_chip == Chip.O
    board.turn(3, Chip.O)
    assert board.next_chip == Chip.X
    board.turn(5, Chip.X)
    assert board.next_chip == Chip.O
    board.turn(4, Chip.O)
    assert board.next_chip == Chip.X
    board.turn(9, Chip.X)
    assert board.next_chip is None


def test_turn_should_raise_error_if_invalid_chip_set():
    board = Board()
    with pytest.raises(WrongTurnError):
        board.turn(1, Chip.O)
    board.turn(2, Chip.X)
    with pytest.raises(WrongTurnError):
        board.turn(3, Chip.X)


def test_turn_should_raise_error_if_cell_occupied():
    board = Board()
    board.turn(1, Chip.X)
    with pytest.raises(CellOccupiedError):
        board.turn(1, Chip.O)


def test_turn_should_raise_error_if_game_is_over():
    board = Board()
    solve_by_line(Board._first_diagonal_cells(), Chip.X, board)
    assert board.result is GameResult.X
    c = next(board.empty_cells())
    with pytest.raises(GameOverError):
        board.turn(c, Chip.O)


def test_should_be_solved_if_player_wins():
    solve_for_player(Chip.X)
    solve_for_player(Chip.O)


def test_should_be_solved_if_draw():
    board = Board()
    x = Chip.X
    o = Chip.O
    turns = [
        (5, x),
        (3, o),
        (7, x),
        (9, o),
        (6, x),
        (4, o),
        (2, x),
        (8, o),
        (1, x)]
    for (cell, chip) in turns:
        board.turn(cell, chip)
    assert board.result == GameResult.XO
    assert board.next_chip is None


def test_should_abort_the_game():
    board = Board()
    assert board.result is None
    assert board.abort() == GameResult.XO
    assert board.result == GameResult.XO


def test_should_not_abort_the_game_if_game_is_over():
    board = Board()
    solve_by_line(Board._first_diagonal_cells(), Chip.X, board)
    assert board.result == GameResult.X
    assert board.abort() == GameResult.X
    assert board.result == GameResult.X


def test_should_refuse_x():
    board = Board()
    assert board.result is None
    assert board.refuse(Chip.X) == GameResult.O
    assert board.result == GameResult.O


def test_should_refuse_o():
    board = Board()
    assert board.result is None
    assert board.refuse(Chip.O) == GameResult.X
    assert board.result == GameResult.X


def test_should_not_refuse_if_game_is_over():
    board = Board()
    solve_by_line(Board._first_diagonal_cells(), Chip.X, board)
    assert board.result == GameResult.X
    assert board.refuse(Chip.X) == GameResult.X
    assert board.result == GameResult.X


def test_turn_should_return_result_if_game_is_done():
    board = Board()
    board.turn(5, Chip.X)
    board.turn(8, Chip.O)
    board.turn(7, Chip.X)
    board.turn(2, Chip.O)
    board.turn(6, Chip.X)
    board.turn(9, Chip.O)
    board.turn(1, Chip.X)
    board.turn(4, Chip.O)
    r = board.turn(3, Chip.X)
    assert r == GameResult.X
