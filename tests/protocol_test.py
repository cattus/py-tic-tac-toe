import pytest

from tic_tac_toe import GameResult
from tic_tac_toe.protocol import *


class TestTextEncoder:
    e = TextEncoder()

    def test_encode_hello(self):
        assert self.e.encode(HelloMessage(1, 'server info')) == b'hello 1 server info'

    def test_encode_login(self):
        assert self.e.encode(LoginMessage('username', 'password')) == b'login username password'

    def test_encode_user_info(self):
        assert self.e.encode(UserInfoMessage('username', 1, 2, 3)) == b'user username 1 2 3'

    def test_encode_start_game(self):
        assert self.e.encode(StartGameMessage()) == b'start'

    def test_encode_game_started(self):
        assert self.e.encode(GameStartedMessage('x-player-name', 'o-player-name')) == b'game x-player-name o-player-name'

    def test_encode_turn(self):
        assert self.e.encode(TurnMessage(8)) == b'turn 8'

    def test_game_over(self):
        assert GameResult.XO.name == 'XO'
        for x in GameResult:
            assert self.e.encode(GameOverMessage(x)) == f'over {x.name}'.encode()

    def test_error_message(self):
        assert self.e.encode(ErrorMessage('error message')) == b'error error message'


class TestTextDecoder:
    e = TextEncoder()
    d = TextDecoder()

    def check_decode(self, message: Message):
        encoded = self.e.encode(message)
        decoded = self.d.decode(encoded)
        assert self.d.decode(encoded) == message, f"{decoded} doesnt match {message}; encoded sample: {encoded}"

    def test_decode_hello(self):
        self.check_decode(HelloMessage(100500, 'server info'))

    def test_decode_login(self):
        self.check_decode(LoginMessage('username', 'password'))

    def test_decode_user_info(self):
        self.check_decode(UserInfoMessage('user-name', 1, 2, 3))

    def test_decode_start_game(self):
        self.check_decode(StartGameMessage())

    def test_decode_game_started(self):
        self.check_decode(GameStartedMessage('X_player', 'O_player'))

    def test_decode_turn(self):
        self.check_decode(TurnMessage(2))

    def test_decode_game_over(self):
        for x in GameResult:
            self.check_decode(GameOverMessage(x))

    def test_decode_game_over_invalid_result(self):
        with pytest.raises(DecodingError):
            self.d.decode(b'over XOXOXO')

    def test_decode_error_message(self):
        self.check_decode(ErrorMessage("long error message with spaces and #$#.; symbols"))

    def test_should_throw_decoding_error_in_case_of_invalid_command(self):
        with pytest.raises(DecodingError):
            self.d.decode(b'unknown command')


class TestTextProtocol:
    def test_text_protocol_init(self):
        p = TextProtocol()
