
import pytest
import random
import string

from tic_tac_toe.user_service import *


def _random_letters(n=10):
    return ''.join(random.choice(string.ascii_letters) for i in range(n))

def _create_memory_user_service():
    return MemoryUserService(salt_rounds=4)


class TestUserService:
    def test_should_create_user(self):
        us = _create_memory_user_service()
        names = {}
        while len(names) < 5:
            names[_random_letters()] = _random_letters()
        for name, pwd in names.items():
            user_info = us.create_user(name, pwd)
            assert user_info.name == name

    def test_should_not_create_user_if_already_exists(self):
        us = _create_memory_user_service()
        name = _random_letters()
        pwd = _random_letters()
        user_info = us.create_user(name, pwd)
        assert user_info.name == name
        with pytest.raises(UserAlreadyExistsError):
            us.create_user(name, pwd)

    def test_should_find_user_by_credentials(self):
        us = _create_memory_user_service()
        names = {}
        while len(names) < 5:
            names[_random_letters()] = _random_letters()
        users = {}
        for name, pwd in names.items():
            users[name] = us.create_user(name, pwd)
        for name, pwd in names.items():
            assert users[name] == us.find_by_credentials(name, pwd)

    def test_should_not_find_user_when_wrong_password_or_user(self):
        us = _create_memory_user_service()
        names = {}
        while len(names) < 5:
            names[_random_letters()] = _random_letters()
        for name, pwd in names.items():
            us.create_user(name, pwd)
        for name in names:
            assert us.find_by_credentials(name, _random_letters() + _random_letters()) is None
        assert us.find_by_credentials(_random_letters() + _random_letters(), _random_letters()) is None

    def test_should_find_user_by_name(self):
        us = _create_memory_user_service()
        names = {}
        while len(names) < 5:
            names[_random_letters()] = _random_letters()
        users = {}
        for name, pwd in names.items():
            users[name] = us.create_user(name, pwd)
        for name, pwd in names.items():
            assert users[name] == us.find_by_name(name)
        assert us.find_by_name(_random_letters() + _random_letters()) is None

    def test_should_save_game_results(self):
        us = _create_memory_user_service()
        user_info = us.create_user(_random_letters(), _random_letters())
        assert user_info.stats.wins == 0
        assert user_info.stats.loses == 0
        assert user_info.stats.draws == 0
        us.save_game_result(user_info.name, wins=1)
        assert us.find_by_name(user_info.name).stats.wins == 1
        us.save_game_result(user_info.name, loses=2)
        assert us.find_by_name(user_info.name).stats.loses == 2
        us.save_game_result(user_info.name, draws=3, wins=1)
        s = us.find_by_name(user_info.name).stats
        assert s.draws == 3
        assert s.wins == 2
        us.save_game_result(user_info.name, draws=0, wins=0, loses=0)
        s = us.find_by_name(user_info.name).stats
        assert s.draws == 3 and s.wins == 2 and s.loses == 2

    def test_should_raise_error_on_save_game_result_when_user_not_found(self):
        us = _create_memory_user_service()
        with pytest.raises(UserNotFoundError):
            us.save_game_result(_random_letters(), wins=1)


