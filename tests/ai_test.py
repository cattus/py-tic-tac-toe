
import pytest

from asyncio import wait_for
import asyncio

from unittest.mock import MagicMock

from tic_tac_toe import Chip, UserInfo, GameResult
from tic_tac_toe.ai import RandomAIPlayer, BasicAIFactory
from tic_tac_toe.game import Game


class TestRandomAIPlayer:
    def test_should_be_active(self):
        ai = RandomAIPlayer()
        assert ai.is_active() is True

    def test_should_return_player_info(self):
        ai = RandomAIPlayer()
        assert isinstance(ai.player, UserInfo)
        assert len(ai.player.name) > 0

    @pytest.mark.asyncio
    async def test_should_made_turn_on_game_start_if_player_is_x(self):
        game = Game(None, Chip.X)
        event = asyncio.Event()
        game.turn = MagicMock(side_effect=lambda *args: event.set())
        ai = RandomAIPlayer()
        ai.on_game_start(game)
        await wait_for(event.wait(), 1)
        game.turn.assert_called_once()

    @pytest.mark.asyncio
    async def test_should_made_turn_if_opponent_made_turn(self):
        game = Game(None, Chip.O)
        event = asyncio.Event()
        game.turn = MagicMock(side_effect=lambda *args: event.set())
        ai = RandomAIPlayer()
        ai.on_game_start(game)
        ai.on_opponent_turn(5)
        await wait_for(event.wait(), 1)
        game.turn.assert_called_once()

    @pytest.mark.asyncio
    async def test_should_not_made_turn_if_opponent_turn_wins(self):
        game = Game(None, Chip.O)
        game.turn = MagicMock(side_effect=lambda *args: pytest.fail("Should not be called"))
        ai = RandomAIPlayer()
        ai.on_game_start(game)
        ai._board.turn(1, Chip.X)
        ai._board.turn(8, Chip.O)
        ai._board.turn(5, Chip.X)
        ai._board.turn(6, Chip.O)
        ai.on_opponent_turn(9)
        await asyncio.sleep(1/100)
        game.turn.assert_not_called()

    def test_should_not_do_turn_if_game_is_over(self):
        game = Game(None, Chip.O)
        game.turn = MagicMock()
        ai = RandomAIPlayer()
        ai.on_game_start(game)
        ai.on_game_over(GameResult.XO)
        ai.on_opponent_turn(5)
        game.turn.assert_not_called()


class TestBasicAIFactory:
    def test_create_new_ai(self):
        f = BasicAIFactory()
        isinstance(f.create_player(), RandomAIPlayer)
