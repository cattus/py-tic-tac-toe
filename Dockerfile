FROM python:3.7-alpine

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev \
  && apk add libffi-dev py-cffi


ADD tic_tac_toe /tic_tac_toe
ADD requirements.txt /
ADD setup.py /
ADD README.rst /
RUN pip install -r requirements.txt
ENV PYTHONPATH="${PYTHONPATH}:/"
CMD [ "python", "tic_tac_toe/server.py", "0.0.0.0", "9999"]