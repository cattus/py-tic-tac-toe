from io import open

from setuptools import find_packages, setup

with open('tic_tac_toe/__init__.py', 'r') as f:
    for line in f:
        if line.startswith('__version__'):
            version = line.strip().split('=')[1].strip(' \'"')
            break
    else:
        version = '0.0.1'

REQUIRES = [
    'bcrypt',
    'urwid'
]

kwargs = {
    'name': 'tic-tac-toe',
    'version': version,
    'description': 'Example of tic tac toe game server',
    'long_description': 'Example of tic tac toe game server',
    'author': 'Andrey Feoktistov',
    'author_email': 'cattum@gmail.com',
    'maintainer': 'Andrey Feoktistov',
    'maintainer_email': 'cattum@gmail.com',
    'url': 'https://github.com/_/tic-tac-toe',
    'license': 'MIT/Apache-2.0',
    'classifiers': [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
    'install_requires': REQUIRES,
    'tests_require': ['coverage', 'pytest'],
    'packages': find_packages(exclude=('tests', 'tests.*')),

}

setup(**kwargs)
